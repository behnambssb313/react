import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import { Menu, Icon,Row,Col,Layout,Button,Collapse,Input,Card} from 'antd';
import 'antd/dist/antd.css';
import './App.css'
import { BrowserRouter ,Route,Link,Switch,Router} from 'react-router-dom';
import PrivateRoute from './Component/PrivateRoute';
import Userpanel from './Component/Userpanel';

import Login from './Component/auth/Login';
import Home from './Component/Home';
import Dashbord from './Component/ContetnMain/Dashbord'


import axios from 'axios'

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      isAuthenticated:true
    }
  }

componentDidMount(){
  let apiToken=localStorage.getItem('api_token');
  if(apiToken !== null){
    axios.get(`http://roocket.org/api/user?api_token=${apiToken}`)
          .then(response=>{
              this.setState({isAuthenticated:true})
          })
          .catch(error=>{
            this.setState({isAuthenticated:false})
          })
  }
  else{
    this.setState({isAuthenticated : false})
  }
}

handleLogin() {
  this.setState({ isAuthenticated : true});
}
  render(){
    return(
        <Switch>
        <PrivateRoute path="/Dashbord" component={Dashbord} auth={this.state.isAuthenticated}/> 
        <Route path="/login" render={(props) => <Login {...props} auth={this.state.isAuthenticated} />}/> 
        </Switch>
    );
}
}
  
 

export default App;
  