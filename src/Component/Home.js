import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import { Menu, Icon,Row,Col,Layout,Button,Collapse,Input,Card} from 'antd';
import { BrowserRouter ,Route,Link,Switch} from 'react-router-dom';

import HeaderMenu from './HeaderMenu';
import SidebarMenu from './SidebarMenu';

// routes menu sidebar
import  Dashbord from './ContetnMain/Dashbord';
import  Option2 from './ContetnMain/Option2';
import ContentMain from './ContentMain';



const { SubMenu } = Menu;
const { Header, Footer, Sider, Content } = Layout;

const {Panel}=Collapse;
const {Search}=Input;


const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

const customPanelStyle = {
  background: '#f7f7f7',
  borderRadius: 4,
  marginBottom: 24,
  border: 0,
  overflow: 'hidden',
};

class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            articles: [],
            display:'none',
            collapsed:false
        }
    }
    
      toggle = () => {
        if(this.state.display=='none'){
          return this.setState({display:'block' })  
        }
        return this.setState({display:'none' })  
        
      };
    
      toggleCollapsed=()=>{
        this.setState({
          collapsed: !this.state.collapsed,
        });
      }
      
      render() {
        return (
          <div>
            <Layout>
              <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
                <SidebarMenu/>
              </Sider>
              
              <Layout>
                <Header style={{ background: '#fff'}}   >
                  
                    <HeaderMenu stateCollapsed={this.state.collapsed} toggleFunc={this.toggleCollapsed} />
                </Header>
                <Content> 
                    <Row type="flex" justify="space-around"> 
                      <Col span={20}>
                      <Route path="/Dashbord" component={Dashbord}/>
                        <Route path="/Option2" component={Option2}/>
                        <Route path="/ContentMain" component={ContentMain}/>  
                      </Col>
                    </Row>            
                </Content>
              </Layout>  
    
            </Layout>
           
          </div>
        );
      }
}

export default Home;