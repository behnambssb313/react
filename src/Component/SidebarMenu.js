import React,{Component} from 'react';
import { Route,Link} from 'react-router-dom';
import { Menu, Icon, Button } from 'antd';
const { SubMenu } = Menu;



class SidebarMenu extends Component {
  rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];
  state = {
    collapsed: false,
  };
  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  render() {
    return (
      <div>
        <div className="logo" style={{backgroundColor:'#001529',height:'64px'}}></div>
        <Menu
          defaultSelectedKeys={['1']}
          mode="inline"
          theme="dark"
         
        >
          <Menu.Item key="1">
            <Icon type="dashboard"/>
            <span><Link to="/Dashbord" className="sidebar-menu-li-a"  style={{color:'white'}} >پیشخوان</Link></span>
          </Menu.Item>
          <Menu.Item key="2">
            <Icon type="desktop" />
            <span><Link to="/Option2" className="sidebar-menu-li-a" style={{color:'white'}} >خوبی</Link></span>
          </Menu.Item>
          <Menu.Item key="3">
            <Icon type="inbox" />
            <span><Link to="/ContentMain" className="sidebar-menu-li-a" style={{color:'white'}} >Option 3</Link></span>
          </Menu.Item>
          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon type="mail" />
                <span>Navigation One</span>
              </span>
            }
          >
            <Menu.Item key="5">Option 5</Menu.Item>
            <Menu.Item key="6">Option 6</Menu.Item>
            <Menu.Item key="7">Option 7</Menu.Item>
            <Menu.Item key="8">Option 8</Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub2"
            title={
              <span>
                <Icon type="appstore" />
                <span>Navigation Two</span>
              </span>
            }
          >
            <Menu.Item key="9">Option 9</Menu.Item>
            <Menu.Item key="10">Option 10</Menu.Item>
            <SubMenu key="sub3" title="Submenu">
              <Menu.Item key="11">Option 11</Menu.Item>
              <Menu.Item key="12">Option 12</Menu.Item>
            </SubMenu>
          </SubMenu>
        </Menu>
      </div>
    );
  }
}

export default SidebarMenu;
