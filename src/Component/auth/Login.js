import React,{Component, Children} from 'react';
import { Form, Icon, Input, Button, Checkbox ,Row,Col} from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';

class Login extends Component{
    constructor(props){
        super(props);
       
        this.state={
            fields:{
                email:'',
                password:''
            },
            error:{},
            
        }
    }
    handlechange=e=>{
        let fildes=this.state.fields;
        fildes[e.target.name]=e.target.value;
        this.setState({fildes});
       
    }
    handleSubmit = e => {
        e.preventDefault();
        let {email,password}=this.state.fields;

        this.props.form.validateFields((err, values) => {
          if (!err) {
            let data=new FormData();
            data.append('email',email);
            data.append('password',password);
           
            axios.post('http://roocket.org/api/login',data)
                .then(response=>{
                    localStorage.setItem('api_token',response.data.data.api_token);
                    this.props.history.push('/');
                    console.log(this.props);
                })
                .catch(error=>{
                    console.log(error)
                })
          }
           
        });
      };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
        
        <body style={{width:'100%',height:'100%'}} >
                <Row style={{display:'flex',alignItems:'center',justifyContent:'center',height:'100%',backgroundColor:'#f3f3f3'}}>
            <Col span={8}></Col>
            <Col style={{backgroundColor:'#fff',boxShadow:'0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',padding:'100px'}} span={8}>
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('email', {
                            rules: [
                                {type:'email', message: 'فرمت ایمیل نامعتبر است'},
                                { required: true, message: 'ایمیل نمی تواند خالی بماند' }
                            ],
                        })(
                            <Input
                            onChange={this.handlechange.bind(this)}
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="نام کاربری"
                            name="email"
                            type="email"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item style={{marginTop:'20px'}}>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'رمزعبور نمی تواند خالی بماند' }],
                        })(
                            <Input
                            onChange={this.handlechange.bind(this)}
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            name="password"
                            placeholder="رمز عبور"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(<Checkbox>Remember me</Checkbox>)}
                        <a className="login-form-forgot" href="">
                            Forgot password
                        </a>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Log in
                        </Button>
                        Or <a href="">register now!</a>
                    </Form.Item>
                </Form>
            </Col>
            <Col span={8}></Col>
        </Row>
        </body>
     );
    
  }
}
const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login);
export default WrappedNormalLoginForm;