import React,{Component} from 'react';
import { Row,Col,Menu,Button,Icon,Badge,Avatar,Dropdown} from 'antd';

const menu = (
  <Menu>
    <Menu.Item key="0">
      <a href="http://www.alipay.com/">
       تغییر رمزعبور
      </a>
    </Menu.Item>
    <Menu.Item key="1">
      <a href="http://www.taobao.com/">
       ویرایش حساب
      </a>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="3" >
      تنظیمات
    </Menu.Item>
  </Menu>
);

class HeaderMenu extends Component{
    render(){
        return(
            <Row >
             
             <Col xl={22}>
              <Badge dot status="processing"/>
                <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" size={'large'} />
              <Dropdown overlay={menu} >
                <a className="ant-dropdown-link" href="#" style={{margin:'5px 5px 0px 0px',fontFamily:'IRANSans',float:'left'}}>
                  بهنام سرخوش<Icon type="down"/>
                </a>
             </Dropdown>
             </Col>
             <Col xl={2} style={{textAlign:'right'}}>
               
               <Icon style={{textAlign:'right',fontSize:'20px'}}
                type={this.props.stateCollapsed ? 'menu-unfold' : 'menu-fold'}
                onClick={this.props.toggleFunc}
            
              />
               {/* {console.log(this.props.nx)} */}
              </Col>
            </Row>
        );
    }
}

export default HeaderMenu;