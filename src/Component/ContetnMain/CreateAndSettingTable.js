import React,{Component} from 'react';
import { Drawer, Form, Button, Col, Row, Input, Select, DatePicker, Icon } from 'antd';

import DrawerSetting from './DrawerSetting';
import DrawerCreate from './DrawerCreate';

class CreateAndSettingTable extends Component{
    state = { visible: false,visible2:false };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };
  showDrawer2 = () => {
    this.setState({
      visible2: true,
    });
  };
  onClose = () => {
    this.setState({
      visible: false,
    });
  };
  onClose2 = () => {
    this.setState({
      visible2: false,
    });
  };

    render(){
        return(
            <div>
            <Row type="flex" justify="start">
                <Col span={2}>
                    <Button type="primary" onClick={this.showDrawer2} style={{width:'100%',lineHeight:'2'}}>
                    <Icon type="plus" />جدید
                    </Button>
                </Col>
                <Col span={2}>
                    <Button type="primary" onClick={this.showDrawer} style={{width:'100%',marginRight:'5px',lineHeight:'2'}}>
                        <Icon type="setting"  />تنظیمات
                    </Button>
                </Col>
            </Row>
 
            <Drawer
                title="تنظیمات"
                style={{height:'140px'}}
                width={720}
                onClose={this.onClose}
                visible={this.state.visible}
                placement={'top'}
                style={{textAlign:'right'}}
            >
                <DrawerCreate/>
            </Drawer>
            <Drawer
                title="ایجاد رکورد جدید"
                width={720}
                onClose={this.onClose2}
                visible={this.state.visible2}
                placement={'bottom'}
                style={{textAlign:'right'}}
            >
                <DrawerSetting/>
            </Drawer>

            </div>
        );
    }
}

export default CreateAndSettingTable;