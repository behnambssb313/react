import React,{Component} from 'react';
import { Table, Icon, Switch, Radio, Form, Divider,Checkbox,Dropdown,Input} from 'antd';


const FormItem = Form.Item;

const expandedRowRender = record => <p>{record.description}</p>;
const title = () => 'Here is title';
const showHeader = true;
const scroll = { y: 240 };
const pagination = { position: 'bottom' };




class DrawerSetting extends Component{

    render(){
        return(
            <div>{console.log(this.props.sizeProps)}
      <Form layout="inline">
            <FormItem label="حاشیه جدول">
              <Checkbox checked={this.props.borderedProps} onChange={this.props.handleToggleProp('bordered')} />
            </FormItem>
            <FormItem label="توضیحات">
              <Switch checked={!!this.props.expandedRowRendeProps} onChange={this.props.handleExpandChangeProps} />
            </FormItem>
            <FormItem label="انتخاب">
              <Switch checked={!!this.props.rowSelectionProps} onChange={this.props.handleRowSelectionChangeProps} />
            </FormItem>
            <FormItem label="اسکرول">
              <Switch checked={!!this.props.scrollProps} onChange={this.props.handleScollChangeProps} />
            </FormItem>
            <FormItem label="اندازه جدول">
              <Radio.Group size="default" value={this.props.sizeProps} onChange={this.props.handleSizeChangeProps}>
                <Radio.Button value="default">Default</Radio.Button>
                <Radio.Button value="middle">Middle</Radio.Button>
                <Radio.Button value="small">Small</Radio.Button>
              </Radio.Group>
            </FormItem>
            
          </Form>

       
            </div>
        );
    }
}

export default DrawerSetting;