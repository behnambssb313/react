import React,{Component} from 'react';
import { Table, Icon, Switch, Radio, Form, Divider,Layout,Checkbox,Dropdown,Menu,Row,Col,Button,Collapse,Input,Card,Drawer} from 'antd';
import CreateAndSettingTable from './ContetnMain/CreateAndSettingTable';
import axios from 'axios';
import faker from 'faker';
import { object } from 'prop-types';

import DrawerSetting from './ContetnMain/DrawerSetting'
import DrawerCreate from './ContetnMain/DrawerCreate';

const { SubMenu } = Menu;
const { Header, Footer, Sider, Content } = Layout;
const {Panel}=Collapse;
const {Search}=Input;
const FormItem = Form.Item;







// colums table
const columns = [
  {
    title: 'نام',
    dataIndex: 'name',
    align:'center',
    key: 'name',
    width: 100,
    render: text => <a href="javascript:;">{text}</a>,
  },
  {
    title: ' نام کاربری',
    dataIndex: 'username',
    align:'center',
    key: 'username',
    width: 150,
    render: text => <a href="javascript:;">{text}</a>,
  },
  {
    title: 'ایمیل',
    dataIndex: 'email',
    align:'center',
    key: 'email',
    width: 70,
  },
  {
    title: 'رمز عبور',
    align:'center',
    dataIndex: 'password',
    key: 'password',
    width: 200,
  },
  {
    title: 'فعالیت ها',
    align:'center',
    key: 'action',
    width: 360,
    render: (text, record) => (
      <span>
        <a href="javascript:;">Action 一 {record.name}</a>
        <Divider type="vertical" />
        <a href="javascript:;">Delete</a>
        <Divider type="vertical" />
        <a href="javascript:;" className="ant-dropdown-link">
          More actions <Icon type="down" />
        </a>
      </span>
    ),
  },
];
const data = [];
for (let i = 1; i <= 30; i++) {
  data.push({
    key: i,
    name: 'John',
    Lname: 'Brown',
    age: `${i}2`,
    address: `New York No. ${i} Lake Park`,
    description: `My name is John Brown, I am ${i}2 years old, living in New York No. ${i} Lake Park.`,
  });
}

const expandedRowRender = record => <p>{record.description}</p>;
const title = () => 'Here is title';
const showHeader = true;
const scroll = { y: 240 };
const pagination = { position: 'bottom' };
let datas=[];
const db=[];


class ContentMain extends React.Component {

  state = {
    bordered: false,
    loading: false,
    pagination,
    size: 'default',
    expandedRowRender,
    title: undefined,
    showHeader,
    rowSelection: {},
    scroll: undefined,
    hasData: true,
    display:'none',
    articles:'',
    visible: false,
    visible2:false
  };

// get api
  componentDidMount(){
    axios.get('https://jsonplaceholder.ir/users')
         .then(response=>{
            datas=response.data;
            datas.map((current)=>{
              db.push({
                key: current.id,
                name: current.name,
                username:current.username,
                email:current.email,
                password: current.address.city,
                description: `My name is John Brown, I am 2 years old, living in New York No.  Lake Park.`,
              })
            })
            console.log(db);  });
}

//drawer
showDrawer = () => {
  this.setState({
    visible: true,
  });
};
showDrawer2 = () => {
  this.setState({
    visible2: true,
  });
};
onClose = () => {
  this.setState({
    visible: false,
  });
};
onClose2 = () => {
  this.setState({
    visible2: false,
  });
};



// settings table evant
  handleToggle = prop => enable => {
    this.setState({ [prop]: enable });
  };

  handleSizeChange = e => {
    this.setState({ size: e.target.value });
  };

  handleExpandChange = enable => {
    this.setState({ expandedRowRender: enable ? expandedRowRender : undefined });
  }; 

  handleRowSelectionChange = enable => {
    this.setState({ rowSelection: enable ? {} : undefined });
  };

  handleScollChange = enable => {
    this.setState({ scroll: enable ? scroll : undefined });
  };

  handlePaginationChange = e => {
    const { value } = e.target;
    this.setState({
      pagination: value === 'none' ? false : { position: value },
    });
  };

  toggle = () => {
    if(this.state.display=='none'){
      return this.setState({display:'block' })  
    }
    return this.setState({display:'none' })  
  };

  render() {
    const { state } = this;
    return (
      <div>
         <Row type="flex" justify="center" style={{marginTop:'80px'}}>
                  <Col span={20}>
                    <Row type="flex" justify="start">
                  <Col span={2}>
                      <Button type="primary" onClick={this.showDrawer2} style={{width:'100%',lineHeight:'2'}}>
                      <Icon type="plus" />جدید
                      </Button>
                  </Col>
                  <Col span={2}>
                      <Button type="primary" onClick={this.showDrawer} style={{width:'100%',marginRight:'5px',lineHeight:'2'}}>
                          <Icon type="setting"  />تنظیمات
                      </Button>
                  </Col>
              </Row>
              <Drawer
                title="تنظیمات"
                width={720}
                onClose={this.onClose}
                visible={this.state.visible}
                placement={'top'}
                style={{textAlign:'right'}}
            >
                  <Form layout="inline">
            <FormItem label="حاشیه جدول">
              <Switch checked={state.bordered} onChange={this.handleToggle('bordered')} />
            </FormItem>
            <FormItem label="توضیحات">
              <Switch checked={!!state.expandedRowRender} onChange={this.handleExpandChange} />
            </FormItem>
            <FormItem label="انتخاب">
              <Switch checked={!!state.rowSelection} onChange={this.handleRowSelectionChange} />
            </FormItem>
            <FormItem label="اسکرول">
              <Switch checked={!!state.scroll} onChange={this.handleScollChange} />
            </FormItem>
            <FormItem label="اندازه جدول">
              <Radio.Group size="default" value={state.size} onChange={this.handleSizeChange}>
                <Radio.Button value="default">Default</Radio.Button>
                <Radio.Button value="middle">Middle</Radio.Button>
                <Radio.Button value="small">Small</Radio.Button>
              </Radio.Group>
            </FormItem>
            
          </Form>

            </Drawer>
            <Drawer
                title="ایجاد رکورد جدید"
                width={720}
                onClose={this.onClose2}
                visible={this.state.visible2}
                placement={'bottom'}
                style={{textAlign:'right'}}
            >
                <DrawerCreate/>
            </Drawer>

                    {/* <CreateAndSettingTable/> */}
                    <Search placeholder="بگرد دنبال ..." onSearch={value => console.log(value)} size="large" key="1"/>
                    <Card style={{display:`${this.state.display}`}}>
                       <p>Card content</p>
                       <p>Card content</p>
                       <p>Card content</p>
                     </Card>
                     <Button size={"large"} type="primary" key={1} key="2" onClick={this.toggle} block>جستجوی پیشرفته</Button>
                     <Table {...this.state} columns={columns} dataSource={state.hasData ? db  : null} /> 
                  </Col>
               </Row>
       
       
      </div>
    );
  }
}




export default ContentMain;