import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

import { ConfigProvider  } from 'antd';
import faIR from 'antd/es/locale-provider/fa_IR';

import 'mdbreact/dist/css/mdb.css'
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route, NavLink, Switch } from 'react-router-dom'


ReactDOM.render(
    <ConfigProvider locale={faIR}>
<BrowserRouter>
    <App />
</BrowserRouter>
    </ConfigProvider>
, document.getElementById('root'));

serviceWorker.unregister();
    